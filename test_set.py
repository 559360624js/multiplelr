from sklearn import linear_model
import train_set as mlr
import matplotlib.pyplot as plt
import pandas as pd
import csv

dataset = list(csv.reader(open('testdatalist.csv')))
listofdata = dataset[1:]
df = pd.DataFrame(listofdata,columns=dataset[0])
solution = mlr.train_set()

n = len(dataset)-1

output_val = '_rubber_export_km'
reg_val = ['_currency_exchange_rate']
non_reg_val = ['oil_price','rubber_produce_10tt']
error = {}
for country in solution:
	input_head = []
	input_head += non_reg_val
	for val in reg_val:
		input_head += [country+val]
	regr = solution[country][0]
	error_sum = 0
	for i in range(n):
		new_dflist = list(map(float,df[input_head].iloc[i].tolist()))
		predict_val = regr.predict([new_dflist])[0]
		test_val = float(df[country+output_val][i])
		error_percent = abs((predict_val-test_val)/test_val)*100
		error_sum += error_percent
	error[country] = error_sum/n
# for val in error:
	# print(val+':',error[val],'%')
# mlr.show_solution()
mlr.save_solution()