from sklearn import linear_model
import matplotlib.pyplot as plt
import pandas as pd
import csv
import json

def train_set():
	dataset = list(csv.reader(open('datalist.csv')))
	listofdata = dataset[1:]
	df = pd.DataFrame(listofdata,columns=dataset[0])

	output_val = '_rubber_export_km'
	reg = ['china','japan','malaysia','usa']
	reg_val = ['_currency_exchange_rate']
	non_reg_val = ['oil_price','rubber_produce_10tt']
	solution = {}

	for country in reg:
		output_head = [country + output_val]
		input_head = []
		input_head += non_reg_val
		for val in reg_val:
			input_head += [country+val]
		x = df[input_head]
		y = df[output_head[0]]
		regr = linear_model.LinearRegression()
		regr.fit(x,y)
		
		intercept = regr.intercept_
		coef = regr.coef_
		equation = 'y='
		for i in range(len(coef)):
			equation += '('+str(coef[i])+')'+'x'+str(i+1)+'+('
		equation += str(intercept)+')'
		solution[country] = [regr,equation]
	return solution
def show_solution():
	solution = train_set()
	for val in solution:
		print(val+':'+solution[val][1])
def save_solution():
	# f=open("final_solution.json","w")
	solution = train_set()
	json_solution = {}
	for val in solution:
		regr = solution[val][0]
		json_solution[val] = {'intercept':regr.intercept_,\
		'coef':regr.coef_.tolist()}
	print(json_solution)
	f = open("final_solution.json","w")
	json.dump(json_solution,f)
show_solution()